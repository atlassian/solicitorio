"use strict";
const chalk = require("chalk");
const fs = require('fs');
const { resolve } = require("path");
const arePackagesInstalled = require("./lib/arePackagesInstalled");
const loadSettings = require("./lib/loadSettings");
const isWhitelisted = require("./lib/isWhitelisted");
const isInTheList = require("./lib/isInTheList");
const loadDependencies = require("./lib/loadDependencies");

/**
 * Main Solicitorio function.
 * @param {string} path - Directory that will be scanned by this tool.
 * @param {object} flags - Options passed in via the command line e.g. ignore-versions
 */
module.exports = async function main(path, flags) {
    const resolvedPath = resolve(path);

    if (flags.verbose) {
        console.log(':: Checking path:', resolvedPath)
    }

    try {
        const { compatibleLicenses, warningLicenses, whiteListedModules } =
            loadSettings(resolvedPath);

        const nodePackages = Object.entries(
            await loadDependencies(resolvedPath, !flags.includeDevelopment)
        );

        if (!await arePackagesInstalled(resolvedPath, nodePackages.length, flags.yes)) {
            process.exit(3);
            return;
        }

        preprocessLicenses(nodePackages);

        const compatibleModules = nodePackages.filter(
            ([name, nodePackage]) =>
                isInTheList(nodePackage, compatibleLicenses) ||
                isWhitelisted(name, whiteListedModules, flags.ignoreVersions)
        );

        const warningModules = nodePackages.filter(([, nodePackage]) =>
            isInTheList(nodePackage, warningLicenses)
        );
        const totalModuleCount = nodePackages.length;
        const incompatibleModules = nodePackages.filter(
            (nodePackageEntry) => !compatibleModules.includes(nodePackageEntry)
        );

        if (!incompatibleModules.length && !warningModules.length) {
            console.log(
                chalk.green(
                    `All ${totalModuleCount} licenses are compliant [${resolvedPath}]`
                )
            );
        }

        if (incompatibleModules.length) {
            console.log(
                chalk.red(
                    `Found ${incompatibleModules.length} out of ${totalModuleCount} modules incompatible for use by us [${resolvedPath}]`
                )
            );
            incompatibleModules.forEach(([name, nodePackage]) =>
                console.log(
                    chalk.red(
                        getNodePackageDescription(name, nodePackage)
                    )
                )
            );
        }

        if (warningModules.length) {
            console.log(
                chalk.yellow(
                    `Found ${warningModules.length} licenses flagged as warning [${resolvedPath}]`
                )
            );
            warningModules.forEach(([name, nodePackage]) =>
                console.log(
                    chalk.yellow(
                        getNodePackageDescription(name, nodePackage)
                    )
                )
            );
        }

        if (flags.verbose) {
            compatibleModules.forEach((module) => console.log(chalk.blue(`Compliant: ${module[0]} ${module[1].licensesOriginal}`)));
        }

        if (incompatibleModules.length) {
            process.exit(1);
        }
    } catch (error) {
        console.error(chalk.red(`Error in [${resolvedPath}] Is there a package.json file in that folder? The error was:\n`), error);
        process.exit(2);
    }
};

function getNodePackageDescription(name, nodePackage) {
    const licenseFilePart = nodePackage.licenseFile ? nodePackage.licenseFile : "(no license file found)";
    return `${name} ${nodePackage.licensesOriginal} ${licenseFilePart}`;
}

function preprocessLicenses(nodePackages) {
    nodePackages.forEach(([packageName, nodePackage]) => {
        nodePackage.licensesOriginal = nodePackage.licenses;

        if (Array.isArray(nodePackage.licenses)) {
            const dualLicensed = isDualLicensed(nodePackage);
            const joiningOperator = dualLicensed ? "OR" : "AND";

            nodePackage.licenses = nodePackage.licenses.join(` ${joiningOperator} `);
            if (dualLicensed) {
                nodePackage.licensesOriginal += " (dual-licensed)";
            }
        }

        // Remove leading and trailing whitespace and enclosing parentheses
        nodePackage.licenses = nodePackage.licenses.trim().replace(/^\((.*)\)$/, "$1").trim();

        // Detect logical operators
        const logicalOperatorDetectionRegex = /\s+(?:AND|OR)\s+/gi;
        const detectionMatches = nodePackage.licenses.match(logicalOperatorDetectionRegex);
        if (detectionMatches) {
            // Parse logical operators (trivial scenario when all the operators are the same i.e. a OR b, a OR b OR c, etc.)
            const allOperatorsSame = detectionMatches.every((operator) => operator === detectionMatches[0]);
            if (allOperatorsSame) {
                nodePackage.licensesOperator = detectionMatches[0].toUpperCase().trim();
                nodePackage.licensesParsed = nodePackage.licenses.split(logicalOperatorDetectionRegex);
                return;
            }

            throw new Error(`Unsupported license logical expression: ${packageName}, '${nodePackage.licensesOriginal}'`)
        }
    });
}
function isDualLicensed(nodePackage) {
    if (nodePackage.licenses.length > 1 && nodePackage.licenseFile) {
        const licenseFileContent = fs.readFileSync(nodePackage.licenseFile, "utf8");
        const dualLicensedRegex = /Dual licensed under the/;

        return licenseFileContent.match(dualLicensedRegex);
    } else {
        return false;
    }
}
