# Changelog

## [3.4.2](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.4.2%0D3.4.1#diff)

### Fixes
- Ensure ISC* licenses extracted from LICENSE and README files are considered compliant (DCA11Y-1642)

## [3.4.1](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.4.1%0D3.4.0#diff)

### Fixes
- Improve how we communicate around the expected number of packages when the `package.json` file is missing a 'version'
  specification (DCA11Y-1460)
- Add a missing test for `cli` operations to ensure potential problems like those with the recent `meow` renovate updates are not accepted again


## [3.4.0](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.4.0%0D3.3.1#diff)

### Fixes

- Address Solicitorio VULNs VULN-1432915 and VULN-1432762
  - Update `glob` for `read-package-json` to avoid dependency on inflight
  - Update test-only `package.json` file to avoid false positive jquery vulnerabilities

## [3.3.1](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.3.1%0D3.3.0#diff)

### Fixes

- Fixed DCA11Y-1291 where the warning prompt about a potentially alarming number of packages found was causing false positive problems in CI pipelines for projects without any production dependencies. This was fixed by calculating the minimum expected number of packages based on the `package.json` file contents. 

## [3.3.0](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.3.0%0D3.2.5#diff)

### Features

- Added reminder message about installing packages before running Solicitiorio.
- Added prompt when the number of license-checker results is lesser than 2 to double-check with the user if all packages are installed.
- Added --yes parameter to bypass prompts

## [3.2.5](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.2.5%0D3.2.4#diff)

### Fixes

- Fix problems found during the extended testing of 3.2.4 (accidental release)

## [3.2.4](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.2.4%0D3.2.3#diff)

### Notice

- Released accidentally before extended manual testing instead of a hash release

### Fixes

- Support simple AND and OR license expressions (DCA11Y-1196)
- Avoid invalid or unknown licenses and license expressions from being allowed when an allowed license happens to be their substring

### Changed

- Make system tests faster and debuggable by avoiding sub-processes
- Overall test improvements to reduce duplication and improve readability 
- Automate patch creation with `npm run prepare-patches`

## [3.2.3](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.2.3%0D3.2.2#diff)

### Fixes

- Improved handling of BlueOak and Apache licenses (DCA11Y-1197)

## [3.2.2](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.2.2%0D3.2.1#diff)

### Fixes

- Improved handling of Atlassian licenses (DCA11Y-1118):
  - the links in README don't take precedence over LICENSE.txt
  - the newer form (https://www.atlassian.com/legal/atlassian-customer-agreement) is allowed
  - the older form (https://www.atlassian.com/legal/software-license-agreement) is allowed with a warning

## [3.2.1](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.2.1%0D3.2.0#diff)

### Fixes

- "Apache 2" is no longer reported as incompatible (DCA11Y-1115)

## [3.2.0](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.2.0%0D3.1.4#diff)

- Modify license checking for private packages, by warning about "unknown" licenses in private packages
  - Previously, private packages with unknown licenses were set as "UNLICENSED" and marked compliant, now they are 
    checked and a warning is reported if the license is unknown to license-checker or if there's no license
  - Licenses that are not matched in the license-checker list are now reported as-is, instead of being set as "UNKNOWN"
    This means that they are then checked against our list of allowed licenses afterwards.
  - "UNLICENSED" license now reports a warning, as it implies no open-source license, which means that use of the code
    is disallowed
- Added tests for the whole solicitorio process with our patched license-checker

## [3.1.4](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.1.4%0D3.1.3#diff)

### Changed

- Add TinyMCE to allowlist, as we pay for a commercial license and the included GPL-2.0 license doesn't apply to us

## [3.1.3](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.1.3%0D3.1.2#diff)

### Changed

- use Jest snapshots on licenseList (to draw attention to its content and disallow unintentional changes)

## [3.1.2](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.1.2%0D3.1.1#diff)

### Fixes

- DCA11Y-1064: 'W3C' is allowed
- 
## [3.1.1](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.1.1%0D3.1.0#diff)

### Fixes

- DCA11Y-1064: 'BSD' is allowed

## [3.1.0](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.1.0%0D3.0.3#diff)

### Features

- DCA11Y-906: read license info also from private packages

### Fixes

- run `patch-package` only in DEV mode

### Changed

- show package and path info in verbose mode

## [3.0.3](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.0.3%0D3.0.2#diff)

### Bug fixes

- bundle patched `license-checker` into the served package


## [3.0.2](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.0.2%0D3.0.1#diff)

### Features

-   Added verbose mode

### Bug Fixes

- DCA11Y-999 - Analyse extraneous dependencies for node projects with workspaces

## [3.0.1](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.0.1%0D3.0.0#diff)

### Bug Fixes

- JSMDC-20736 Fix: regex special character escape
- Add Apache*, BSD* and python 2.0 licenses to allowlist

## [3.0.0](https://bitbucket.org/atlassian/solicitorio/branches/compare/3.0.0%0D2.0.1#diff)

### Breaking changes

-   Ignore development dependencies by default

### Features

-   Update license list
-   Give a friendlier warning when asked to scan a folder without a module

## [2.0.1](https://bitbucket.org/atlassian/solicitorio/branches/compare/2.0.1%0D2.0.0#diff)

### Bug Fixes

-   Prevent reporting its own dependencies by including CC-BY 2.0, 3.0, and 4.0 which are allowed

## [2.0.0](https://bitbucket.org/atlassian/solicitorio/branches/compare/2.0.0%0D1.0.0#diff)

### Breaking changes

-   Remove repository configurable license lists. This is to ensure Legal has checked licenses

## [1.0.0](https://bitbucket.org/atlassian/solicitorio/branches/compare/1.0.0%0D0.0.4#diff) (2022-03-17)

### Breaking changes

-   Only supporting Node 10 or higher

### Features

-   Removed outdated npm-license-crawler dependency
-   Removed dependency on underscore
-   Jest tests
-   JSDoc

### Bug Fixes

-   Upgraded to jsonfile@6.1.0
-   Upgraded to meow@9.0.0
-   Upgraded to license-checker@25.0.1

## [0.0.4](https://bitbucket.org/atlassian/solicitorio/branches/compare/0.0.4%0D0.0.3#diff) (2021-01-22)

### Features

-   Message for missing .solicitoriorc file added
-   Lockfile added to pin dependencies

## [0.0.3](https://bitbucket.org/atlassian/solicitorio/branches/compare/0.0.3%0D0.0.2#diff) (2018-04-19)

## [0.0.2](https://bitbucket.org/atlassian/solicitorio/branches/compare/0.0.2%0Dv0.0.1#diff) (2018-01-03)

## [0.0.1](https://bitbucket.org/atlassian/solicitorio/branches/compare/0.0.2%0Dv0.0.1#diff) (2016-01-20)

Universe began.
