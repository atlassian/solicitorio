#!/usr/bin/env node
"use strict";
const meow = require("meow");
const solicitorio = require("./index");
const packInfo = require("./package.json");

const cli = meow(
    `
    Usage
      $ solicitorio [--help] [--version] [directory...]

    Options
      --help                Display usage instructions
      --ignore-versions     Ignore the versions of the whitelisted dependencies. [Default: false]
      --include-development Check the licenses of development dependencies. [Default: false]
      --yes                 Answer all appearing boolean prompts with yes. [Default: false]
      directory...          One or more directories

    Examples
      $ solicitorio --help
      $ solicitorio --version
      $ solicitorio --include-development
      $ solicitorio --yes
      $ solicitorio .
      $ solicitorio /src/ponies /src/rainbows
`,
    {
        flags: {
            ignoreVersions: {
                type: "boolean",
            },
            includeDevelopment: {
                type: "boolean",
            },
            verbose: {
                type: "boolean",
            },
            yes: {
                type: "boolean",
            }
        },
    }
);

if (cli.flags.verbose) {
    console.log(`${packInfo.name} ${packInfo.version}`);
}

cli.input.forEach((directory) => {
    solicitorio(directory, cli.flags);
});
