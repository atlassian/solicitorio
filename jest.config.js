const { defaults } = require('jest-config');

module.exports = {
    ...defaults,
    clearMocks: true, // Calls jest.clearAllMocks() between every test
    restoreMocks: true, // Calls jest.restoreAllMocks() between every test
    globalSetup: `${__dirname}/test/globalSetup.js`,
};
