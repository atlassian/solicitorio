# @atlassian/solicitorio

> Scans given directory for npm dependencies with unusable licenses


## Add to a project

```
$ npm install --save-dev @atlassian/solicitorio
```

```
Usage
  $ ./node_modules/.bin/solicitorio [--help] [--version] [directory...]
```


## Use the CLI in any project

Use with npx
```
$ npx @atlassian/solicitorio [--help] [--version] [directory...]
```

or install globally
```
$ npm install --global @atlassian/solicitorio
```

```
$ solicitorio --help

  Usage
    $ solicitorio [--help] [--version] [directory...]

  Options
    --help                Display usage instructions
    --ignore-versions     Ignore the versions of the whitelisted dependencies. [Default: false]
    --include-development Check the licenses of development dependencies. [Default: false]
    directory...          One or more directories
```

Any licenses that are not in the compatibleLicenses or warningLicenses array will throw an error.


## Contributing

The crucial part of the package is its [list of allowed licences](lib/licenseList.js).
Be careful! And better check with Legal Team before intentionally changing them!

### Running tests
`npm test`

### Releasing
1. Ensure your changes bump the version in package.json
2. Run the release step of the pipeline that was automatically started *after* the merge to master
3. Apply the proper version-tag on the commit from which that release pipeline was run

## License

MIT © [Marco De Jongh](http://atlassian.com)
