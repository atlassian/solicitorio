"use strict";
const createLicenseRegex = require("./createLicenseRegex");

/**
 * Checks the license of a node package against a list licenses. License is matched if it is a substring of a compatible license, irregardless of case.
 * @param {{ licenses: string }} nodePackage - Node package.
 * @param {string[]} licenseList - List of compatible license strings.
 * @returns {boolean} Whether the license is compatible or not.
 */
module.exports = function isInTheList(nodePackage, licenseList) {
    if (
        !Array.isArray(licenseList) ||
        licenseList.length === 0 ||
        !nodePackage.licenses
    ) {
        return false;
    }

    const regex = createLicenseRegex(licenseList);
    if (nodePackage.licensesOperator) {
        if (nodePackage.licensesOperator === "OR") {
            return nodePackage.licensesParsed.some((license) => regex.test(license));
        } else if (nodePackage.licensesOperator === "AND") {
            return nodePackage.licensesParsed.every((license) => regex.test(license));
        } else {
            throw new Error(`Unsupported license operator: ${nodePackage.licensesOperator}:${nodePackage.licensesParsed} '${nodePackage.licenses}'`);
        }
    } else {
        return regex.test(nodePackage.licenses);
    }
};
