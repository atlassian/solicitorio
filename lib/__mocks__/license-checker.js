"use strict";

const checker = jest.createMockFromModule("license-checker");

let mockDependencies = {};
let mockError = false;

function __setMockDependencies(dependencies) {
    mockDependencies = dependencies;
}

function __setMockError(error) {
    mockError = error;
}

function fakeInit(options, cb) {
    if (mockError) {
        cb(mockError, []);
    } else {
        cb(undefined, mockDependencies);
    }
}

checker.__setMockDependencies = __setMockDependencies;
checker.__setMockError = __setMockError;
checker.init = fakeInit;

module.exports = checker;
