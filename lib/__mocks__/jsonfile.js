"use strict";

const jsonfile = jest.createMockFromModule("jsonfile");

// This is a custom function that our tests can use during setup to specify
// what the files on the "mock" filesystem should look like when any of the
// `jsonfile` APIs are used.
let mockFiles = {};
function __setMockFiles(newMockFiles) {
    mockFiles = newMockFiles;
}

// A custom version of `readFileSync` that reads from the special mocked out
// file list set via __setMockFiles
function readFileSync(path) {
    if (!(path in mockFiles)) {
        let myError = new Error("mock error from lib/__mocks__/jsonfile");
        myError.errno = -2;
        throw myError;
    }
    return mockFiles[path];
}

jsonfile.__setMockFiles = __setMockFiles;
jsonfile.readFileSync = readFileSync;

module.exports = jsonfile;
