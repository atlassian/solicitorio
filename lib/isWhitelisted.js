"use strict";

/**
 * Checks the name of a node package against a list of whitelisted modules.
 * @param {string} name - Node package name including version.
 * @param {string[]} whitelist - List of whitelisted modules including version.
 * @param {boolean} ignoreVersions - Ignore the version number of the node package.
 * @returns {boolean} Whether the node package is whitelisted or not.
 */
function isWhitelisted(name, whitelist, ignoreVersions) {
    if (!Array.isArray(whitelist) || whitelist.length === 0) {
        return false;
    }
    if (ignoreVersions) {
        // This needs to consider scoped package name like @atlassian/solicitorio@1.0.0
        const nameWithoutVersion = name.replace(/@[^@]+$/, "");
        return whitelist.some((moduleName) =>
            moduleName.startsWith(nameWithoutVersion)
        );
    } else {
        return whitelist.includes(name);
    }
}

module.exports = isWhitelisted;
