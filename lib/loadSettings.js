"use strict";
const licenses = require("./licenseList")

/**
 * Loads default settings based on licenseList.js
 * @returns {object} A settings object that contains all compatible and warning licenses and whitelisted modules.
 */
function loadSettings() {
    try {
        if (
            !licenses.compatibleLicenses ||
            !licenses.warningLicenses ||
            !licenses.whiteListedModules
        ) {
            throw new Error(
                "Make sure all required fields are present in licenseList.js"
            );
        }
        return {
            compatibleLicenses: [
                ...licenses.compatibleLicenses,
                ...licenses.warningLicenses,
            ],
            warningLicenses: licenses.warningLicenses,
            whiteListedModules: licenses.whiteListedModules,
        };
    } catch (e) {
        throw new Error(`Error processing licenseList.js: ${e}`);
    }
}
module.exports = loadSettings;
