const licenses = {
    /**
     * These licenses are for distributed code software which has stricter
     * requirements than non-distributed software (e.g. SaasS). If needed in the
     * future we support both use-cases.
     */
    compatibleLicenses: [
        "AFL-2.1",
        "AFLv2.1",
        "ANTLR-PD",
        "Apache2",
        "Apache 2",
        "Apache 2.0",
        "Apache-1.1",
        "Apache-2.0",
        "Apache*", // when license-checker does not find a license in package.json, it returns Apache* in case of Apache License
        "Apache License, Version 2.0", // see DCA11Y-1197
        "BlueOak-1.0.0", // see DCA11Y-1197
        "BSL-1.0",
        "0BSD",
        "BSD", // see DCA11Y-1064
        "BSD3", // DCA11Y-1196 backbone-query-parameters@0.2.2
        "BSD-new", // DCA11Y-1196 backbone-query-parameters@0.2.2
        "New-BSD", // DCA11Y-1196 historyjs@1.8.0-b2
        "BSD-2-Clause",
        "BSD-3-Clause",
        "BSD*", // when license-checker does not find a standard license in package.json, it returns BSD* in case of BSD License
        "bzip2-1.0.6",
        "CC-BY-1.0",
        "CC-BY-2.0",
        "CC-BY-3.0",
        "CC-BY-4.0",
        "CC0-1.0",
        "Custom: https://www.atlassian.com/legal/atlassian-customer-agreement",
        "curl",
        "FTL",
        "GD",
        "ImageMagick",
        "Info-ZIP",
        "ISC",
        "ISC*", // when license-checker does not find a license in package.json, it returns ISC* in case of ISC License found in readme or license files
        "LPPL-1.3c",
        "libpng-2.0",
        "MIT",
        "MIT*", // when license-checker does not find a license in package.json, it returns MIT* in case of MIT License found in readme or license files
        "Net-SNMP",
        "NIST-Software",
        "Nunit",
        "OLDAP-2.8",
        "Python-2.0",
        "SSH-OpenSSH",
        "OpenSSL",
        "PHP-3.01",
        "Plexus",
        "PostgreSQL",
        "PSF-2.0",
        "Public Domain",
        "OFL-1.0",
        "snprintf",
        "Unlicense",
        "W3C-20150513",
        "W3C", // see DCA11Y-958
        "WTFPL",
        "X11",
        "Zlib",
        "ZPL-2.0",
    ],
    warningLicenses: [
        "Artistic-1.0",
        "Artistic-2.0",
        "CDDL-1.0",
        "CPAL-1.0",
        "CPL-1.0",
        // Give more visibility - should change to https://www.atlassian.com/legal/atlassian-customer-agreement
        "Custom: https://www.atlassian.com/legal/software-license-agreement",
        "EPL-1.0",
        "EPL-2.0",
        "GPL-2.0-with-classpath-exception",
        "LGPL-2.1",
        "LGPL-2.1-only",
        "LGPL-3.0",
        "LGPL-3.0-only",
        "LGPL-3.0-or-later",
        "MPL-1.1",
        "MPL-2.0",
        "NPL-1.1",
        "UNLICENSED", // Code labelled UNLICENSED is not open source, and we should not be using it (if it's not Atlassian code)
    ],
    // Make sure to re-enable the 'Whitelisted modules with different versions (not ignoring their versions) do not override incompatible licenses' test if you ever whitelist a specific version of a module
    whiteListedModules: [
        "@atlaskit/brand-logos",
        "@atlaskit/eslint-plugin-design-system",
        "@atlaskit/icon",
        "tinymce", // We use TinyMCE with a commercial license, but it gets reported as "GPL-2.0-or-later"
    ],
};

module.exports = licenses;
