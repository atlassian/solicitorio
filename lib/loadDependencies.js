"use strict";
const checker = require("license-checker");

/**
 * Loads the packages.
 * @param {string} path - Root directory of the project for which to load dependencies.
 * @param {boolean} productionOnly - Include all or only production dependencies
 * @returns {Promise<ModuleInfos>} Promise resolving into a map of packages and their details.
 */
function loadDependencies(path, productionOnly) {
    return new Promise((resolve, reject) => {
        checker.init({ start: path, production: productionOnly }, function (err, packages) {
            if (err) {
                reject(err);
            } else {
                resolve(packages);
            }
        });
    });
}
module.exports = loadDependencies;
