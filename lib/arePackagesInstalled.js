"use strict";
const chalk = require("chalk");
const fs = require('fs');
const path = require("path");
const readline = require("readline");
/**
 * Checks if packages are installed and reminds user to use updated packages.
 * @param {string} fullPath - Root directory of the project where node_modules and package.json will be checked.
 * @param {number} packagesCount - Number of packages found by Solicitorio.
 * @param {boolean} shouldApprovePrompt - If true, prompt will be automatically approved.
 * @returns {boolean} Boolean value that indicates if packages are installed and updated.
 */
const arePackagesInstalled = async (fullPath, packagesCount, shouldApprovePrompt) => {
    console.log(
        chalk.yellow("Please remember to install/update packages, otherwise the results may be incorrect.")
    );

    const packageJsonPath = path.join(fullPath, "package.json");
    const packageJsonData = JSON.parse(fs.readFileSync(packageJsonPath, "utf8"));

    if (packageJsonData.version === undefined) {
        console.warn(`Warning: ${packageJsonPath} does not specify a 'version' which will result in a decreased package count`);
    }

    const projectSelfCount = 1;
    const dependenciesCount = packageJsonData.dependencies ? Object.keys(packageJsonData.dependencies).length : 0;
    const isWorkspaceCountModifier = packageJsonData.workspaces !== undefined ? 1 : 0;
    const expectedPackagesCount = projectSelfCount + dependenciesCount + isWorkspaceCountModifier;

    console.log(`Number of packages found by Solicitorio: ${packagesCount} (expected: ${expectedPackagesCount}+).`);

    if (packagesCount >= expectedPackagesCount) {
        return true;
    }

    if (!fs.existsSync(path.join(fullPath, "node_modules"))) {
        console.log(chalk.yellow("node_modules directory is missing."));
    }

    const promptQuestion = "Are you sure, that all packages are installed? (y/n): "
    if (shouldApprovePrompt) {
        console.log(promptQuestion + "yes");
        return true;
    } else {
        return await booleanPrompt(promptQuestion);
    }
};

const booleanPrompt = (question) => {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    return new Promise(resolve => rl.question(question, ans => {
        rl.close();
        const answerLowerCase = ans.toLowerCase();
        resolve(answerLowerCase === "y" || answerLowerCase === "yes");
    }));
}
module.exports = arePackagesInstalled;
