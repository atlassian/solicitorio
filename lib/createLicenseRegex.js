"use strict";

/**
 * Creates a regex expression from a list.
 * @param {array} licenses - List of license strings.
 * @returns {RegExp} Regex expression of licenses concatenated with '|', case insensitive.
 */
function createLicenseRegex(licenses) {
    // This concatenates the licenses with '|' to create the regex
    // The replace regex escapes the special characters that may be in the license names
    return new RegExp(licenses
        .map(license => license.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'))
        .map(licenseRegex => `^${licenseRegex}$`)
        .join("|"), "i");
}

module.exports = createLicenseRegex;
