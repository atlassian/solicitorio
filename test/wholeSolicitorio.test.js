const fs = require('fs');
const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const arePackagesValid = require("../lib/arePackagesInstalled");
jest.mock("../lib/arePackagesInstalled");

// Override the mock with the real module
jest.mock('license-checker', () => jest.requireActual('license-checker'));
const solicitorio = require("../index");

describe("test solicitorio with patched license-checker", () => {
    const tempDir = path.join(__dirname, 'temp_project');
    let mockExitSpy;
    let consoleOutput = [];
    const storeLog = (log) => consoleOutput.push(log);
    const DEPENDENCIES = {
        "fakePackageCompatible": "file:../fakePackageCompatibleNoDeps",
        "fakePackageIncompatible": "file:../fakePackageIncompatible",
        "fakePackageUnknown": "file:../fakePackageUnknown"
    };
    const packageJson = {
        name: "temp_project",
        version: "1.0.0",
        license: "UNLICENSED",
        dependencies: DEPENDENCIES,
    };
    arePackagesValid.mockImplementation(() => Promise.resolve(true));

    beforeAll(async () => {
        // create a fake package to test dependencies of
        fs.mkdirSync(tempDir);
        fs.writeFileSync(
            path.join(tempDir, 'package.json'),
            JSON.stringify(packageJson, null, 2)
        );
        await exec('npm install', { cwd: tempDir });
    });

    afterAll(() => {
        // remove the fake package
        fs.rmSync(tempDir, { recursive: true, force: true });
    });

    beforeEach(() => {
        jest.resetModules(); // Clears any module mocks
        mockExitSpy = jest.spyOn(process, "exit").mockImplementation(() => {});
        // store console.log calls to assert on later
        jest.spyOn(console, "log").mockImplementation(storeLog);
        jest.spyOn(console, "error").mockImplementation(storeLog);
        consoleOutput = []; // Reset for each test
    });

    test("Process exits with 1 (unknown license is incompatible, known license is Compliant)", async () => {
        expect.assertions(7);
        const flags = { verbose: true };
        await solicitorio(tempDir, flags);
        expect(mockExitSpy).toHaveBeenCalledTimes(1);
        const containsIncompatibleMessage = consoleOutput.some(log => log.includes("Found 1 out of 4 modules incompatible for use by us"));
        const containsWarningMessage = consoleOutput.some(log => log.includes("Found 2 licenses flagged as warning"));
        const containsCompatibleMessage = consoleOutput.some(log => log.includes("Compliant: fakePackageCompatible@0.0.0 MIT"));
        const containsUnlicensedMessage = consoleOutput.some(log => log.includes("temp_project@1.0.0 UNLICENSED (no license file found)"));
        const containsFakePackageMessage = consoleOutput.some(log => log.includes("fakePackageUnknown@0.0.0 BAD FAKE NO GOOD UNKNOWN LICENSE (no license file found)"));
        const incompatiblePackage = consoleOutput.some(log => log.includes("fakePackageIncompatible@0.0.0 LGPL-3.0 (no license file found)"));
        expect(containsIncompatibleMessage).toBe(true);
        expect(containsWarningMessage).toBe(true);
        expect(containsCompatibleMessage).toBe(true);
        expect(containsFakePackageMessage).toBe(true);
        expect(incompatiblePackage).toBe(true);
        expect(containsUnlicensedMessage).toBe(true);
    });
});
