const { exec } = require("child_process");
const { mkdirSync, rmdirSync } = require("fs");
const path = require("path");

async function prepareTest22(projectDirectory) {
    const lockName = ".solicitorio-fnm.lock";
    const lockFilepath = path.join(__dirname, lockName);

    let lockObtained = false;
    while (!lockObtained) {
        try {
            mkdirSync(lockFilepath);
            lockObtained = true;
        } catch (error) {
            await delay(100);
        }
    }

    try {
        await runCommand("fnm use v22 && npm run test:prepare", projectDirectory);
    } finally {
        rmdirSync(lockFilepath);
    }
}

async function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const runCommand = (command, cwd) => {
    return new Promise((resolve, reject) => {
        exec(command, { cwd }, (error, stdout, stderr) => {
            if (error) {
                reject(error);
            } else {
                resolve({ stdout, stderr });
            }
        });
    });
};

module.exports = {
    prepareTest22,
    runCommand
}
