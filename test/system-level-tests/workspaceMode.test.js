const path = require('path');
const { prepareTest22 } = require('./_system_utils');
const loadDependencies = require("../../lib/loadDependencies");

jest.unmock('license-checker');
jest.unmock('jsonfile');

jest.setTimeout(10*1000);

describe("workspaceMode tests", () => {
    describe('when there are `workspaces` in `package.json`', () => {
        test('`workspaceMode` is automatically detected, and report contains deep project tree', async () => {
            const fullPath = path.resolve(__dirname, 'test-project_workspaceMode');
            await prepareTest22(fullPath);

            const json = await loadDependencies(fullPath, false);

            expect(Object.keys(json).length).toBe(16);
        });
    });

    describe('when no `workspaces` in `package.json`', () => {
        test('when `workspaceMode` is manually set to `false`, should check only root project', async () => {
            const fullPath = path.resolve(__dirname, 'test-project_workspaceMode-off');
            await prepareTest22(fullPath);

            const json = await loadDependencies(fullPath, false);

            expect(Object.keys(json).length).toBe(2);
        });
    });
})
