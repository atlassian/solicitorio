const path = require('path');
const { prepareTest22 } = require('./_system_utils');
const loadDependencies = require("../../lib/loadDependencies");

jest.unmock('license-checker');
jest.unmock('jsonfile');

jest.setTimeout(10*1000);

describe("privatePackages tests", () => {
    test('all packages should be private AND NOT `UNLICENSED`', async () => {
        const fullPath = path.resolve(__dirname, 'test-project_privatePackages');
        await prepareTest22(fullPath);

        const json = await loadDependencies(fullPath, false);

        expect(
            Object.keys(json).every(pkg => json[pkg].private && json[pkg].licenses !== 'UNLICENSED')
        ).toBe(true)
    });
})
