const solicitorio = require("../../index");
const path = require("path");
const { getMockedConsoleOutput, getChalkRedCode } = require("../_utils");
const { runCommand } = require("./_system_utils");

jest.unmock('license-checker');
jest.unmock('jsonfile');

jest.setTimeout(10*1000);

describe("After fixes to", () => {
    const flags = { "ignore-versions": false, "verbose": true, "yes": true };
    let mockExitSpy;

    beforeEach(() => {
        mockExitSpy = jest.spyOn(process, "exit").mockImplementation(() => {});
        jest.spyOn(console, "log").mockImplementation(() => {});
        jest.spyOn(console, "error").mockImplementation(() => {});
    });

    test("DCA11Y-1197 - should support both BlueOak and Apache", async () => {
        const fullPath = path.resolve(__dirname, "test-DCA11Y-1197");
        await runCommand("npm install", fullPath);

        await solicitorio(fullPath, flags);
        const solicitorioOutput = getMockedConsoleOutput();

        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(solicitorioOutput).toMatch(/All 21 licenses are compliant/);
        expect(solicitorioOutput).toMatch(/Compliant: jackspeak@2\.3\.6 BlueOak-1\.0\.0/);
        expect(solicitorioOutput).toMatch(/Compliant: rx-lite@4\.0\.8 Apache License, Version 2\.0/);
    });

    test("DCA11Y-1196 - should issue error on MIT and GPL for simple-sidebar", async () => {
        const fullPath = path.resolve(__dirname, "test-DCA11Y-1196-MIT-and-GPL");
        await runCommand("npm install", fullPath);

        await solicitorio(fullPath, flags);
        const solicitorioOutput = getMockedConsoleOutput();

        const redChalkCode = getChalkRedCode();
        expect(mockExitSpy).toHaveBeenCalledTimes(1);
        expect(solicitorioOutput).toMatch(/Found 1 out of \d+ modules incompatible for use by us/);
        expect(solicitorioOutput).toMatch(`${redChalkCode}simpler-sidebar@1\.4\.5 (MIT and GPL-2\.0)`);
    });

    test("DCA11Y-1196 - should not print 'undefined' when reporting an array license without a readme present", async () => {
        const fullPath = path.resolve(__dirname, "test-DCA11Y-1196-urijs-dual-without-readme");

        await solicitorio(fullPath, flags);
        const solicitorioOutput = getMockedConsoleOutput();

        const redChalkCode = getChalkRedCode();
        expect(mockExitSpy).toHaveBeenCalledTimes(1);
        expect(solicitorioOutput).toMatch(/Found 1 out of \d+ modules incompatible for use by us/);
        expect(solicitorioOutput).toMatch(`${redChalkCode}urijs@1.14.1 MIT,GPL (no license file found)`);
    });

    test("DCA11Y-1291 - should not prompt for packages with no production dependencies", async () => {
        const fullPath = path.resolve(__dirname, "test-DCA11Y-1291-no-production-dependencies");
        await runCommand("npm install", fullPath);

        await solicitorio(fullPath, flags);
        const solicitorioOutput = getMockedConsoleOutput();

        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(solicitorioOutput).not.toContain("Are you sure, that all packages are installed?");
        expect(solicitorioOutput).toContain("Found 1 licenses flagged as warning");
        expect(solicitorioOutput).toContain("Compliant: @atlassian/dc.product.confluence-content-plugins.status-macro@0.0.0 UNLICENSED");
    });

    test("DCA11Y-1291 - should properly estimate and communicate the found and expected minimum amount of dependencies", async () => {
        const fullPath = path.resolve(__dirname, "test-DCA11Y-1291-many-production-dependencies");
        await runCommand("npm install", fullPath);

        await solicitorio(fullPath, flags);
        const solicitorioOutput = getMockedConsoleOutput();

        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(solicitorioOutput).toContain("Number of packages found by Solicitorio: 4 (expected: 3+).");
    });

    test("DCA11Y-1291 - should use an increased expected count for workspaces", async () => {
        const fullPath = path.resolve(__dirname, "test-DCA11Y-1291-workspace");
        await runCommand("npm install", fullPath);

        await solicitorio(fullPath, flags);
        const solicitorioOutput = getMockedConsoleOutput();

        expect(mockExitSpy).toHaveBeenCalledTimes(0); // No error because we pass --yes
        expect(solicitorioOutput).toContain("Are you sure, that all packages are installed?");
        expect(solicitorioOutput).toContain("Number of packages found by Solicitorio: 1 (expected: 2+).");
    });

    test("DCA11Y-1642 - should consider extracted ISC* as allowed for js-client", async () => {
        const fullPath = path.resolve(__dirname, "test-DCA11Y-1642-statsig-js-client");
        await runCommand("npm install", fullPath);

        await solicitorio(fullPath, flags);
        const solicitorioOutput = getMockedConsoleOutput();

        expect(solicitorioOutput).toContain("Compliant: @statsig/js-client@3.12.2 ISC*");
    });
});
