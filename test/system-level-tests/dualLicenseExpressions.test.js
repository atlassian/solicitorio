const solicitorio = require("../../index");
const path = require("path");
const { getMockedConsoleOutput } = require("../_utils");
const { runCommand } = require("./_system_utils");

jest.unmock('license-checker');
jest.unmock('jsonfile');

jest.setTimeout(10*1000);

describe("Dual license wording in readme files", () => {
    const flags = { "ignore-versions": false, "verbose": true };
    let mockExitSpy;

    beforeEach(() => {
        mockExitSpy = jest.spyOn(process, "exit").mockImplementation(() => {});
        jest.spyOn(console, "log").mockImplementation(() => {});
        jest.spyOn(console, "error").mockImplementation(() => {});
    });

    test("should cause us to use an OR operator to join deprecated license arrays", async () => {
        const fullPath = path.resolve(__dirname, "test-DCA11Y-1196-allowed-dual-license-expressions");
        await runCommand("npm install", fullPath);

        await solicitorio(fullPath, flags);
        const solicitorioOutput = getMockedConsoleOutput();

        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(solicitorioOutput).toMatch(/Compliant: tablesorter@2\.17\.8 MIT,GPL \(dual-licensed\)/);
    });
});
