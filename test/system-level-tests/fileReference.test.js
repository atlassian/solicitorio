const path = require("path");
const loadDependencies = require("../../lib/loadDependencies");
const { prepareTest22 } = require("./_system_utils");

jest.unmock('license-checker');
jest.unmock('jsonfile');

jest.setTimeout(10*1000);

describe("package.json with file reference", () => {
    test("should take the URL from the reference rather than from README", async () => {
        const fullPath = path.resolve(__dirname, "test-project_fileReference");
        await prepareTest22(fullPath);

        const result = await loadDependencies(fullPath, false);

        expect(result["test-project.fileReference@1.0.0"].licenses).toBe(
            "Custom: https://www.atlassian.com/legal/atlassian-customer-agreement",
        );
    });
});
