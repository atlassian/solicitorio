const solicitorio = require("../../index");
const path = require("path");
const { getMockedConsoleOutput, getChalkRedCode } = require("../_utils");
const { runCommand } = require("./_system_utils");

jest.unmock('license-checker');
jest.unmock('jsonfile');

jest.setTimeout(10*1000);

describe("License expressions implemented with DCA11Y-1196", () => {
    const flags = { "ignore-versions": false, "verbose": true };
    let mockExitSpy;

    beforeEach(() => {
        mockExitSpy = jest.spyOn(process, "exit").mockImplementation(() => {});
        jest.spyOn(console, "log").mockImplementation(() => {});
        jest.spyOn(console, "error").mockImplementation(() => {});
    });

    test("should support AND and OR operators", async () => {
        const fullPath = path.resolve(__dirname, "test-DCA11Y-1196");
        await runCommand("npm install", fullPath);

        await solicitorio(fullPath, flags);
        const solicitorioOutput = getMockedConsoleOutput();

        const redChalkCode = getChalkRedCode();
        expect(mockExitSpy).toHaveBeenCalledTimes(1);
        expect(solicitorioOutput).toMatch(/Found 3 out of \d+ modules incompatible for use by us/);
        expect(solicitorioOutput).toMatch(`${redChalkCode}formatchange@2\.3\.1 MIT AND GPL-2.0-or-later`);
        expect(solicitorioOutput).toMatch(`${redChalkCode}jqplot@1\.0\.9 \(MIT AND GPL-2\.0\)`);
        expect(solicitorioOutput).toMatch(`${redChalkCode}simpler-sidebar@1\.4\.5 \(MIT and GPL-2\.0\)`);
        expect(solicitorioOutput).toMatch(/Compliant: plantcode@0\.2\.1 \(MIT OR Apache-2\.0\)/);
        expect(solicitorioOutput).toMatch(/Compliant: react-resize-observer@1.1.1 \(MIT OR CC0-1\.0\)/);
        expect(solicitorioOutput).toMatch(/Compliant: tablesorter@2.32.0 \(MIT OR GPL-2\.0\)/);
    });
});
