const createLicenseRegex = require("../lib/createLicenseRegex");

describe("createLicenseRegex tests", () => {
    test("Creates regex from list with one license", () => {
        const licenses = ["BSD"];
        expect(createLicenseRegex(licenses)).toStrictEqual(/^BSD$/i);
    });

    test("Creates regex from list with multiple licenses", () => {
        const licenses = [
            "BSD",
            "Common Development and Distribution License (CDDL)",
            "Common Public License ",
            "Creative Commons CC0 1.0 Universal License",
            "Eclipse Public License",
            "ISC License",
            "JSON License",
            "LGPL",
            "MIT",
            "Mozilla Public License",
            "Public Domain",
            "Unlicense",
            "W3C License",
            "WTFPL",
            "CC0-1.0",
            "CC-BY-3.0",
            "CC-BY-4.0",
            "ISC",
            "MPL-2.0",
        ];
        expect(createLicenseRegex(licenses)).toStrictEqual(
            /^BSD$|^Common Development and Distribution License \(CDDL\)$|^Common Public License $|^Creative Commons CC0 1\.0 Universal License$|^Eclipse Public License$|^ISC License$|^JSON License$|^LGPL$|^MIT$|^Mozilla Public License$|^Public Domain$|^Unlicense$|^W3C License$|^WTFPL$|^CC0-1\.0$|^CC-BY-3\.0$|^CC-BY-4\.0$|^ISC$|^MPL-2\.0$/i
        );
    });

    test("Creates regex from empty list", () => {
        const licenses = [];
        expect(createLicenseRegex(licenses)).toStrictEqual(/(?:)/i);
    });
});
