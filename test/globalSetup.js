const { runCommand } = require("./system-level-tests/_system_utils");

module.exports = async (globalConfig) => {
    console.log(`Performing global setup, test pattern: '${globalConfig.testPathPattern}'`);

    console.log("Pre-fetching node v22 ...");
    await runCommand("fnm use --install-if-missing v22", __dirname); // Used by workspace mode tests

    console.log("Global setup complete");
};
