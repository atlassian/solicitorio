const chalk = require("chalk");

const getMockedConsoleOutput = () => {
    return console.log.mock.calls.concat(console.error.mock.calls)
        .map((call) => call.join(""))
        .join('\n');
};

const getChalkYellowCode = () => {
    return chalk.yellow('').slice(0, -1); // Remove the last character to get the prefix
};

const getChalkRedCode = () => {
    return chalk.red('').slice(0, -1); // Remove the last character to get the prefix
};

module.exports = {
    getChalkYellowCode,
    getChalkRedCode,
    getMockedConsoleOutput
}
