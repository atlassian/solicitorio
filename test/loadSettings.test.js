const loadSettings = require('../lib/loadSettings');

/**
 * ## Context
 *
 * The Jest Snapshot tests provided in here are used
 * to draw attention to the importance of the `lib/licenseList.js`!
 *
 *
 * ## Intentional changes to the list of licences
 *
 * In future test runs, Jest suite will compare the output of the `lib/licenseList.js` file
 * against the stored snapshot.
 *
 * If there's a mismatch, Jest will report a failure, indicating that the list has changed.
 * If the change _is intentional_, you can update the snapshot by running `npx jest -u`.
 *
 * This command tells Jest to update any snapshots with the current test outputs.
 *
 * Then, make sure to commit them to GIT so that the change would keep the  CI green.
 */
describe('loadSettings', () => {
    let licenceList
    let settingsObject

    beforeAll(() => {
        licenceList = require('../lib/licenseList');
        settingsObject = loadSettings();
    });

    test('returns an object with `compatibleLicenses`, `warningLicenses` and `whiteListedModules`', () => {
        ['compatibleLicenses', 'warningLicenses', 'whiteListedModules'].forEach(key => {
            expect(settingsObject).toHaveProperty(key);
            expect(settingsObject[key]).toBeInstanceOf(Array);
        })
    });

    test('The loaded `licenseList` matches the expected snapshot', () => {
        expect(licenceList).toMatchSnapshot();
    });

    test('`compatibleLicenses` is combined with `warningLicenses` and matches the expected snapshot', () => {
        expect(settingsObject.compatibleLicenses).toEqual([...licenceList.compatibleLicenses, ...licenceList.warningLicenses]);
        expect(settingsObject.compatibleLicenses).toMatchSnapshot();
    });
})
