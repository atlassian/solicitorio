const isInTheList = require("../lib/isInTheList");

describe("isInTheList tests", () => {
    let compatibleLicenses;

    beforeEach(function () {
        compatibleLicenses = [
            "BSD",
            "Common Development and Distribution License (CDDL)",
            "Common Public License",
            "Creative Commons CC0 1.0 Universal License",
            "Eclipse Public License",
            "ISC License",
            "JSON License",
            "LGPL",
            "MIT",
            "Mozilla Public License",
            "Public Domain",
            "Unlicense",
            "W3C License",
            "WTFPL",
            "CC0-1.0",
            "CC-BY-3.0",
            "CC-BY-4.0",
            "ISC",
            "MPL-2.0",
            "Apache*",
        ];
    });

    test("Returns true when license is compatible", () => {
        const nodePackage = {
            licenses: "MIT",
            licenseFile: "/code/node_modules/@babel/code-frame/LICENSE",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeTruthy();
    });

    test("Returns false when license is not compatible", () => {
        const nodePackage = {
            licenses: "Apache-2.0",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeFalsy();
    });

    test("Returns false when license happens to contain a substring that is a compatibile license", () => {
        const nodePackage = {
            licenses: "FalseBSD-GPL-Alternative",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeFalsy();
    });

    test("Returns false when license happens to contain a separate word substring that is a compatibile license", () => {
        const nodePackage = {
            licenses: "False BSD GPL Alternative",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeFalsy();
    });

    test("Returns false to make sure license regex is properly escaped", () => {{
        const nodePackage = {
            licenses: "Apachee", // Added an extra 'e'
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeFalsy();
    }})

    test("Returns true when compatible license has spaces in the string", () => {
        const nodePackage = {
            licenses: "Eclipse Public License",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeTruthy();
    });

    test("Returns false when nodePackage does not have a license field", () => {
        const nodePackage = {
            version: "2.0.3",
            resolved:
                "https://packages.atlassian.com/api/npm/npm-remote/tslib/-/tslib-2.0.3.tgz",
            integrity:
                "sha512-uZtkfKblCEQtZKBF6EBXVZeQNl82yqtDQdv+eck8u7tdPxjLu2/lp5/uPW+um2tpuxINHWy3GhiccY7QgEaVHQ==",
            moduleName:
                "@atlaskit/analytics-next-stable-react-context/node_modules/tslib",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeFalsy();
    });

    test("Returns false when compatible list is empty", () => {
        compatibleLicenses = [];
        const nodePackage = {
            licenses: "0BSD",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeFalsy();
    });

    test("Throws for an invalid operator", () => {
        const nodePackage = {
            licenses: "(BSD XOR GPL)",
            licensesParsed: ["BSD", "GPL"],
            licensesOperator: "XOR",
        };
        expect(() => isInTheList(nodePackage, compatibleLicenses)).toThrow("Unsupported license operator: XOR:BSD,GPL '(BSD XOR GPL)'");
    });

    test("Returns true for OR with a compatible licenses as the first argument", () => {
        const nodePackage = {
            licenses: "irrelevant",
            licensesParsed: ["BSD", "GPL"],
            licensesOperator: "OR",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeTruthy();
    });

    test("Returns false for AND with a compatible licenses as the second argument", () => {
        const nodePackage = {
            licenses: "irrelevant",
            licensesParsed: ["GPL", "MIT"],
            licensesOperator: "AND",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeFalsy();
    });

    test("Returns false for AND with a compatible licenses as the first argument", () => {
        const nodePackage = {
            licenses: "irrelevant",
            licensesParsed: ["BSD", "GPL"],
            licensesOperator: "AND",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeFalsy();
    });

    test("Returns true for AND with a two compatible licenses", () => {
        const nodePackage = {
            licenses: "irrelevant",
            licensesParsed: ["BSD", "MIT"],
            licensesOperator: "AND",
        };
        expect(isInTheList(nodePackage, compatibleLicenses)).toBeTruthy();
    });
});
