#!/usr/bin/env bash

set -eu

if [ "$#" -ne 3 ] && [ "$#" -ne 4 ]; then
    echo "ERROR: Illegal number of parameters"
    echo "To run manual tests comparing two versions of solicitorio on a given 'dev projects' directory, use the following command (watch out for prompts):"
    echo "$0 <old version> <new version> <dev projects directory - ie. directory where other projects are located> [optional: no-install]"
    exit 1
fi

old_version=$1
new_version=$2
dev_projects_dir=$(realpath "$3")
install_flag=${4:-install}

clear

echo "Running manual tests comparing solicitorio versions $old_version and $new_version on dev projects directory $dev_projects_dir ..."
# shellcheck disable=SC2044
for package_json_file in $(find "$dev_projects_dir" -name 'node_modules' -prune -o -name 'test' -prune -o -name 'tests' -prune -o -name 'target*' -prune -o -name 'it' -prune -o -name package.json -print); do
    echo "Checking $package_json_file ..."
    project=$(dirname $package_json_file)
    cd "$project"

    if [ -f "yarn.lock" ]; then
        if [ "$install_flag" = "install" ]; then
            echo "INFO: Running yarn install for $project ..."
            fnm use --install-if-missing
            yarn install --non-interactive || echo "WARNING: yarn install failed for $project"
        else
            echo "INFO: Skipping yarn install for $project"
        fi
    elif [ -f "package-lock.json" ]; then
        if [ "$install_flag" = "install" ]; then
            echo "INFO: Running npm install for $project ..."
            fnm use --install-if-missing
            npm install || echo "WARNING: npm install failed for $project"
        else
            echo "INFO: Skipping npm install for $project"
        fi
    else
        echo "WARNING: Skipping $project because it does not have a yarn.lock or package-lock.json file"
        continue
    fi

    optional_verbose_flag="--verbose" #change to "" if this would cause some trouble
    echo "Running version $old_version ..."
    npx -y "@atlassian/solicitorio@$old_version" . $optional_verbose_flag 2>&1 | sed 's/:[0-9]*:[0-9]*/:line/g' | sed 's#/\.npm/_npx/[^/]*/#/NPX_PATH/#g' | sed 's/node:[0-9]*/node:PID/g' | sed 's#@atlassian/solicitorio [a-z0-9.\-]*#@atlassian/solicitorio VERSION#g' > old_output.txt
    old_exit_code=$?
    old_output=`cat old_output.txt`

    echo "Running version $new_version ..."
    npx -y "@atlassian/solicitorio@$new_version" . $optional_verbose_flag 2>&1 | sed 's/:[0-9]*:[0-9]*/:line/g' | sed 's#/\.npm/_npx/[^/]*/#/NPX_PATH/#g' | sed 's/node:[0-9]*/node:PID/g' | sed 's#@atlassian/solicitorio [a-z0-9.\-]*#@atlassian/solicitorio VERSION#g' > new_output.txt
    new_exit_code=$?
    new_output=`cat new_output.txt`

    wait_for_keypress=false

    if [ "$old_exit_code" -ne "$new_exit_code" ]; then
        echo "WARNING: Exit codes are different for versions $old_version ($old_exit_code) and $new_version ($new_exit_code) on project $project"
        wait_for_keypress=true
    else
        echo "INFO: Exit codes are identical ($new_exit_code)"
    fi

    if [ "$old_output" != "$new_output" ]; then
        echo "WARNING: Outputs are different for versions $old_version (<) and $new_version (>) on project $project"
        echo "< $project/old_output.txt"
        head -n 5 old_output.txt | sed 's/^/< /'
        echo "> $project/new_output.txt"
        head -n 5 new_output.txt | sed 's/^/> /'
        echo "DIFF:"
        diff old_output.txt new_output.txt || true
        wait_for_keypress=true
    else
        echo "INFO: Outputs are identical"
    fi

    if ($wait_for_keypress); then
        read -p "Press any key to continue to the next project ..." -n1 -s
    fi

    rm old_output.txt
    rm new_output.txt

done

echo "Manual tests comparing solicitorio versions $old_version and $new_version on dev projects directory $dev_projects_dir completed"

