const isWhitelisted = require("../lib/isWhitelisted");

describe("isWhitelisted tests", () => {
    let whiteListedModules;

    beforeEach(function () {
        whiteListedModules = [
            "confluence-retention-rules@0.0.0",
            "domutils@1.5.1",
            "@atlassian/brand-logos@1.2.0",
            "format",
            "@atlaskit/icon@21.1.4",
            "@atlaskit/dynamic-table/node_modules/@atlaskit/pagination",
            "@atlaskit/modal-dialog/node_modules/dom-helpers@5.2.1",
        ];
    });

    test("Successfully checks node package with @ symbol in module name", () => {
        const ignoreVersions = true;
        expect(
            isWhitelisted("format@5.2.1", whiteListedModules, ignoreVersions)
        ).toBeTruthy();
    });

    test("Ignoring versions, it returns true when the whitelist contains the module and its version number.", () => {
        const ignoreVersions = true;
        expect(
            isWhitelisted(
                "@atlaskit/modal-dialog/node_modules/dom-helpers@5.2.1",
                whiteListedModules,
                ignoreVersions
            )
        ).toBeTruthy();
    });

    test("Not ignoring versions, it returns true when the whitelist contains the module and its version number.", () => {
        expect(
            isWhitelisted(
                "@atlaskit/modal-dialog/node_modules/dom-helpers@5.2.1",
                whiteListedModules
            )
        ).toBeTruthy();
    });

    test("Ignoring versions, returns true when the whitelist contains the module but different version number.", () => {
        const ignoreVersions = true;
        whiteListedModules = [
            "confluence-retention-rules@0.0.0",
            "domutils@1.5.1",
            "@atlassian/brand-logos@1.2.0",
            "format",
            "@atlaskit/icon@21.1.4",
            "@atlaskit/dynamic-table/node_modules/@atlaskit/pagination",
            "@atlaskit/modal-dialog/node_modules/dom-helpers@5.3.1",
        ];
        expect(
            isWhitelisted(
                "@atlaskit/modal-dialog/node_modules/dom-helpers@5.2.1",
                whiteListedModules,
                ignoreVersions
            )
        ).toBeTruthy();
    });

    test("Not ignoring versions, it returns false when the whitelist contains the module but different version number.", () => {
        whiteListedModules = [
            "confluence-retention-rules@0.0.0",
            "domutils@1.5.1",
            "@atlassian/brand-logos@1.2.0",
            "format",
            "@atlaskit/icon@21.1.4",
            "@atlaskit/dynamic-table/node_modules/@atlaskit/pagination",
            "@atlaskit/modal-dialog/node_modules/dom-helpers@5.3.1",
        ];
        const nodePackage = {
            version: "5.2.1",
            resolved:
                "https://packages.atlassian.com/api/npm/npm-remote/dom-helpers/-/dom-helpers-5.2.1.tgz",
            integrity: "sha1-2UAFNrK/giWtmP4FLgKUUaxA6QI=",
            license: "BSD",
            dependencies: {
                "@babel/runtime": "^7.8.7",
                csstype: "^3.0.2",
            },
            moduleName: "@atlaskit/modal-dialog/node_modules/dom-helpers",
        };
        expect(isWhitelisted(nodePackage, whiteListedModules)).toBeFalsy();
    });

    test("Ignoring versions, it returns false when the whitelist is empty", () => {
        const ignoreVersions = true;
        whiteListedModules = [];
        const nodePackage = {
            version: "5.2.1",
            resolved:
                "https://packages.atlassian.com/api/npm/npm-remote/dom-helpers/-/dom-helpers-5.2.1.tgz",
            integrity: "sha1-2UAFNrK/giWtmP4FLgKUUaxA6QI=",
            license: "BSD",
            dependencies: {
                "@babel/runtime": "^7.8.7",
                csstype: "^3.0.2",
            },
            moduleName: "@atlaskit/modal-dialog/node_modules/dom-helpers",
        };
        expect(
            isWhitelisted(nodePackage, whiteListedModules, ignoreVersions)
        ).toBeFalsy();
    });

    test("Not ignoring versions, it returns false when the whitelist is empty", () => {
        whiteListedModules = [];
        const nodePackage = {
            version: "5.2.1",
            resolved:
                "https://packages.atlassian.com/api/npm/npm-remote/dom-helpers/-/dom-helpers-5.2.1.tgz",
            integrity: "sha1-2UAFNrK/giWtmP4FLgKUUaxA6QI=",
            license: "BSD",
            dependencies: {
                "@babel/runtime": "^7.8.7",
                csstype: "^3.0.2",
            },
            moduleName: "@atlaskit/modal-dialog/node_modules/dom-helpers",
        };
        expect(isWhitelisted(nodePackage, whiteListedModules)).toBeFalsy();
    });
});
