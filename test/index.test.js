const solicitorio = require("../index");
const arePackagesInstalled = require("../lib/arePackagesInstalled");
const { getMockedConsoleOutput } = require("./_utils");
jest.mock("license-checker");
jest.mock("../lib/arePackagesInstalled");

describe("solicitorio integration tests", () => {
    const path = "/path/to";
    const flags = {};
    let mockExitSpy;
    let arePackagesInstalledResult = true;
    arePackagesInstalled.mockImplementation(() => Promise.resolve(arePackagesInstalledResult));

    beforeEach(() => {
        mockExitSpy = jest.spyOn(process, "exit").mockImplementation(() => {});
        jest.spyOn(console, "log").mockImplementation(() => {});
        jest.spyOn(console, "error").mockImplementation(() => {});
        arePackagesInstalledResult = true;
    });

    test("Process exits with 0 (all licenses are compatible)", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "Apache-2.0",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "Apache-2.0",
            },
        };

        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);
        expect.assertions(2);

        flags.ignoreVersions = false;
        await solicitorio(path, flags);
        expect(arePackagesInstalled).toHaveBeenCalledTimes(1);
        expect(mockExitSpy).toHaveBeenCalledTimes(0);
    });

    test("Process exits with 1 (there are incompatible licenses)", async () => {
        const MOCK_DEPENDENCIES = {
            "something@0.0.0": {
                licenses: "LGPL-2.1",
            },
            "another_thing@6.3.8": {
                licenses: "Some Other Bad License",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);
        expect.assertions(1);

        flags.ignoreVersions = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledWith(1);
    });

    test("Process exits with 3 (packages installation check fail)", async () => {
        arePackagesInstalledResult = false;
        await solicitorio(path, flags);
        expect(arePackagesInstalled).toHaveBeenCalledTimes(1);
        expect(mockExitSpy).toHaveBeenCalledTimes(1);
        expect(mockExitSpy).toHaveBeenCalledWith(3);
    });

    test("All licenses are compliant", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "Apache-2.0",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "Apache-2.0",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(console.log.mock.calls.length).toBe(1);
        expect(console.log.mock.calls[0][0]).toContain(
            "All 2 licenses are compliant"
        );
    });

    test("All licenses are incompatible", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "SEE LICENSE IN LICENSE.txt",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "Some Bad License",
                licenseFile:
                    "/code/node_modules/@atlaskit/analytics-next/LICENSE",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledWith(1);
        expect(console.log.mock.calls.length).toBe(3);
        expect(console.log.mock.calls[0][0]).toContain(
            "Found 2 out of 2 modules incompatible for use by us"
        );
        expect(console.log.mock.calls[1][0]).toContain(
            "confluence-retention-rules@0.0.0 SEE LICENSE IN LICENSE.txt"
        );
        expect(console.log.mock.calls[2][0]).toContain(
            "@atlaskit/analytics-next@6.3.8 Some Bad License /code/node_modules/@atlaskit/analytics-next/LICENSE"
        );
    });

    test("Parent module is incompatible, child packages are compatible", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "SEE LICENSE IN LICENSE.txt",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "BSD-3-Clause",
                licenseFile:
                    "/code/node_modules/@atlaskit/analytics-next/LICENSE",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledWith(1);
        expect(console.log.mock.calls.length).toBe(2);
        expect(console.log.mock.calls[0][0]).toContain(
            "Found 1 out of 2 modules incompatible for use by us"
        );
        expect(console.log.mock.calls[1][0]).toContain(
            "confluence-retention-rules@0.0.0 SEE LICENSE IN LICENSE.txt"
        );
    });

    test("Parent module is compatible, at least 1 child packages is incompatible", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "BSD-3-Clause",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "BSD-3-Clause",
                licenseFile:
                    "/code/node_modules/@atlaskit/analytics-next/LICENSE",
            },
            "@babel/helper-annotate-as-pure@7.16.7": {
                licenses: "bad license",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledWith(1);
        expect(console.log.mock.calls.length).toBe(2);
        expect(console.log.mock.calls[0][0]).toContain(
            "Found 1 out of 3 modules incompatible for use by us"
        );
        expect(console.log.mock.calls[1][0]).toContain(
            "@babel/helper-annotate-as-pure@7.16.7 bad license"
        );
    });

    test("Child packages have warning licenses", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "BSD-3-Clause",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "CDDL-1.0",
                licenseFile:
                    "/code/node_modules/@atlaskit/analytics-next/LICENSE",
            },
            "@babel/helper-annotate-as-pure@7.16.7": {
                licenses: "BSD-3-Clause",
            },
            "forwarded@0.1.2": {
                version: "",
                licenses: "CDDL-1.0",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(console.log.mock.calls.length).toBe(3);
        expect(console.log.mock.calls[0][0]).toContain(
            "Found 2 licenses flagged as warning"
        );
        expect(console.log.mock.calls[1][0]).toContain(
            "@atlaskit/analytics-next@6.3.8 CDDL-1.0 /code/node_modules/@atlaskit/analytics-next/LICENSE"
        );
        expect(console.log.mock.calls[2][0]).toContain(
            "forwarded@0.1.2 CDDL-1.0 (no license file found)"
        );
    });

    // Enable back if/when we begin to whitelist a specific version of some module
    test.skip("Whitelisted modules with different versions (not ignoring their versions) do not override incompatible licenses", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "BSD-3-Clause",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "BSD-3-Clause",
                licenseFile:
                    "/code/node_modules/@atlaskit/analytics-next/LICENSE",
            },
            "@atlaskit/brand-logos@1.2.3": {
                licenses: "bad license",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledWith(1);
        expect(console.log.mock.calls.length).toBe(3);
        expect(console.log.mock.calls[0][0]).toContain(
            "Found 1 out of 3 modules incompatible for use by us"
        );
        expect(console.log.mock.calls[1][0]).toContain(
            "@atlaskit/brand-logos@1.2.3 bad license"
        );
    });

    test("Whitelisted modules (ignoring their license versions) override incompatible licenses", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "BSD-3-Clause",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "BSD-3-Clause",
                licenseFile:
                    "/code/node_modules/@atlaskit/analytics-next/LICENSE",
            },
            "@atlaskit/brand-logos@7.16.7": {
                licenses: "bad license",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = true;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(console.log.mock.calls.length).toBe(1);
        expect(console.log.mock.calls[0][0]).toContain(
            "All 3 licenses are compliant"
        );
    });

    test.skip("Empty compatibleLicenses list results in all licenses failing compatibility check", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "BSD",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "BSD",
                licenseFile:
                    "/code/node_modules/@atlaskit/analytics-next/LICENSE",
            },
            "@babel/helper-annotate-as-pure@7.16.7": {
                licenses: "bad license",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = true;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledWith(1);
        expect(console.log.mock.calls.length).toBe(4);
        [
            "Found 2 out of 3 modules incompatible for use by us",
            "confluence-retention-rules@0.0.0 BSD",
            "@atlaskit/analytics-next@6.3.8 BSD /code/node_modules/@atlaskit/analytics-next/LICENSE",
            "Found 0 licenses flagged as warning",
        ].forEach((message, index) => {
            expect(console.log.mock.calls[index][0]).toContain(message);
        });
    });

    test.skip("Warning licenses are added to empty compatibleLicenses list and results in all licenses passing compatibility check", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "MIT",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "MIT",
                licenseFile:
                    "/code/node_modules/@atlaskit/analytics-next/LICENSE",
            },
            "@babel/helper-annotate-as-pure@7.16.7": {
                licenses: "MIT",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = true;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(console.log.mock.calls.length).toBe(2);
        expect(console.log.mock.calls[0][0]).toContain(
            "All 3 licenses are compliant"
        );
        expect(console.log.mock.calls[1][0]).toContain(
            "Found 3 licenses flagged as warning"
        );
    });

    test("Lists all compliant licenses in verbose mode", async () => {
        const MOCK_DEPENDENCIES = {
            "confluence-retention-rules@0.0.0": {
                licenses: "Apache-2.0",
            },
            "@atlaskit/analytics-next@6.3.8": {
                licenses: "Apache-2.0",
            }
        };

        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        flags.verbose = true;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(console.log.mock.calls.length).toBe(4);
        expect(console.log.mock.calls[0][0]).toContain(
            ":: Checking path:"
        );
        expect(console.log.mock.calls[0][1]).toContain(
            "/path/to"
        );
        expect(console.log.mock.calls[1][0]).toContain(
            "All 2 licenses are compliant"
        );
        expect(console.log.mock.calls[2][0]).toContain(
            "Compliant: confluence-retention-rules@0.0.0 Apache-2.0"
        );
        expect(console.log.mock.calls[3][0]).toContain(
            "Compliant: @atlaskit/analytics-next@6.3.8 Apache-2.0"
        );
    });

    // TDD style tests for cases when a new license is to be allowed
    test.each([
        ["BSD - DCA11Y-1064", {
            "magical-component@0.0.0": {
                licenses: "BSD",
            }
        }],
        ["W3C - DCA11Y-958", {
            "magical-component@0.0.0": {
                licenses: "W3C",
            }
        }],
        ["BlueOak - DCA11Y-1197", {
            "jackspeak@2.3.6": {
                licenses: "BlueOak-1.0.0",
            },
        }],
        ["Apache License, Version 2.0 - DCA11Y-1197", {
            "rx-lite@4.0.8": {
                licenses: "Apache License, Version 2.0",
            },
        }],
        ["(MIT) - DCA11Y-1196", {
            "tablesorter-custom@2.32.0": {
                licenses: "(MIT)",
            },
        }],
        ["(MIT OR GPL-2.0) - DCA11Y-1196", {
            "tablesorter@2.32.0": {
                licenses: "(MIT OR GPL-2.0)",
            },
        }],
        ["MIT OR GPL-2.0 - DCA11Y-1196", {
            "tablesorter-custom@2.32.0": {
                licenses: "MIT OR GPL-2.0",
            },
        }],
        ["MIT or GPL-2.0 - DCA11Y-1196", {
            "tablesorter-custom@2.32.0": {
                licenses: "MIT or GPL-2.0",
            },
        }],
        ["(MIT OR CC0-1.0) - DCA11Y-1196", {
            "react-resize-observer@1.1.1": {
                licenses: "(MIT OR CC0-1.0)",
            },
        }],
        ["(MIT OR Apache-2.0) - DCA11Y-1196", {
            "plantcode@0.2.1": {
                licenses: "(MIT OR Apache-2.0)",
            },
        }],
        ["Array[MIT, Apache2] - DCA11Y-1196", {
            "pause-stream@0.0.1": {
                licenses: [
                    "MIT",
                    "Apache2"
                ],
            },
        }],
        ["(BSD-2-Clause OR MIT OR Apache-2.0) - DCA11Y-1196", {
            "rc@1.2.8": {
                licenses: "(BSD-2-Clause OR MIT OR Apache-2.0)",
            },
        }],
        ["BSD3 - DCA11Y-1196", {
            "backbone-query-parameters@0.2.2": {
                licenses: "BSD3",
            },
        }],["BSD-new - DCA11Y-1196", {
            "pixrem@4.0.1": {
                licenses: "BSD-new",
            },
        }],["New-BSD - DCA11Y-1196", {
            "historyjs@1.8.0-b2": {
                licenses: "New-BSD",
            },
        }],["ISC* - DCA11Y-1642", {
            "@statsig/client-core@3.12.2": {
                licenses: "ISC*",
            },
        }],
    ])('%p is allowed', async (description, MOCK_DEPENDENCIES) => {
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        flags.verbose = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(console.log.mock.calls.length).toBe(1);
        expect(console.log.mock.calls[0][0]).toContain(
            "All 1 licenses are compliant"
        );
    });

    // TDD style tests for cases when a warning is to be reported
    test.each([
        ["LGPL-3.0-or-later - DCA11Y-1196", {
            "eslint-plugin-deprecation@1.5.0": {
                licenses: "LGPL-3.0-or-later",
            },
        }],
    ])('%p produces an error', async (description, MOCK_DEPENDENCIES) => {
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        flags.verbose = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledTimes(0);
        expect(console.log.mock.calls.length).toBe(2);
        expect(console.log.mock.calls[0][0]).toContain(
            "Found 1 licenses flagged as warning"
        );
    });

    // TDD style tests for cases when an error is to be reported
    test.each([
        ["(MIT AND GPL-2.0) - DCA11Y-1196", {
            "jqplot@1.0.9": {
                licenses: "(MIT AND GPL-2.0)",
            },
        }],
        ["MIT AND GPL-2.0-or-later - DCA11Y-1196", {
            "formatchange@2.3.1": {
                licenses: "MIT AND GPL-2.0-or-later",
            },
        }],
        ["(MIT and GPL-2.0) - DCA11Y-1196", {
            "simpler-sidebar@1.4.5": {
                licenses: "(MIT and GPL-2.0)",
            },
        }],
        ["Array[MIT, GPL] - DCA11Y-1196", {
            "pause-stream-custom@0.0.1": {
                licenses: [
                    "MIT",
                    "GPL"
                ],
            },
        }],
        ["(BSD-2-Clause AND GPL AND Apache-2.0) - DCA11Y-1196", {
            "simpler-sidebar-custom@1.4.5": {
                licenses: "(BSD-2-Clause AND GPL AND Apache-2.0)",
            },
        }],
    ])('%p produces an error', async (description, MOCK_DEPENDENCIES) => {
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        flags.verbose = false;
        await solicitorio(path, flags);
        expect(mockExitSpy).toHaveBeenCalledTimes(1);
        expect(console.log.mock.calls.length).toBe(2);
        expect(console.log.mock.calls[0][0]).toContain(
            "Found 1 out of 1 modules incompatible for use by us"
        );
    });

    // TDD style tests for cases when an exception is to be thrown due to an invalid license expression
    test.each([
        ["(MIT AND GPL-2.0 OR BSD) - DCA11Y-1196", {
            "jqplot-strange@1.0.9": {
                licenses: "(MIT AND GPL-2.0 OR BSD)",
            },
        }, "jqplot-strange@1.0.9, '(MIT AND GPL-2.0 OR BSD)'"],
    ])('%p produces an exception', async (description, MOCK_DEPENDENCIES, expectedLicenseMsgPart) => {
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);

        flags.ignoreVersions = false;
        flags.verbose = false;
        await solicitorio(path, flags);

        const solicitorioOutput = getMockedConsoleOutput();
        expect(mockExitSpy).toHaveBeenCalledTimes(1);
        expect(solicitorioOutput).toMatch(`Error: Unsupported license logical expression: ${expectedLicenseMsgPart}`);
    });
});
