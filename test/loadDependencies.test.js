jest.mock("license-checker");
const loadDependencies = require("../lib/loadDependencies");

describe("loadDependencies tests", () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    test("Resolves into list of dependencies", async () => {
        const MOCK_DEPENDENCIES = {
            "@ampproject/remapping@2.1.2": {
                licenses: "Apache-2.0",
                licenseFile: "/code/node_modules/@ampproject/remapping/LICENSE",
            },
            "@babel/code-frame@7.16.7": {
                licenses: "MIT",
                licenseFile: "/code/node_modules/@babel/code-frame/LICENSE",
            },
            "caniuse-lite@1.0.30001312": {
                licenses: "CC-BY-4.0",
                licenseFile: "/code/node_modules/caniuse-lite/LICENSE",
            },
            "write-file-atomic@3.0.3": {
                licenses: "ISC",
                licenseFile: "/code/node_modules/write-file-atomic/LICENSE",
            },
            "xml-name-validator@3.0.0": {
                licenses: "Apache-2.0",
                licenseFile:
                    "/code/node_modules/xml-name-validator/LICENSE.txt",
            },
            "xmlchars@2.2.0": {
                licenses: "MIT",
                licenseFile: "/code/node_modules/xmlchars/LICENSE",
            },
            "yargs@16.2.0": {
                licenses: "MIT",
                licenseFile: "/code/node_modules/yargs/LICENSE",
            },
        };
        require("license-checker").__setMockDependencies(MOCK_DEPENDENCIES);
        expect.assertions(1);
        await expect(loadDependencies("/code")).resolves.toEqual(
            MOCK_DEPENDENCIES
        );
    });

    test("Rejects with error if license-checker fails", async () => {
        require("license-checker").__setMockError("Nothing found");
        expect.assertions(1);
        try {
            await loadDependencies("/code");
        } catch (e) {
            expect(e).toEqual("Nothing found");
        }
    });
});
