const path = require('path');
const readline = require("readline");
const arePackagesInstalled = require("../lib/arePackagesInstalled");
const { mkdirSync, rmdirSync } = require("fs");
const { getChalkYellowCode } = require("./_utils");

describe("packages installation validator", () => {
    beforeEach(() => {
        jest.spyOn(console, "log").mockImplementation(() => {});
        jest.spyOn(console, "warn").mockImplementation(() => {});
    });
    afterEach(() => {
        jest.restoreAllMocks();
    });

        let result;
    describe("when number of found packages is GE than expected count", () => {

        beforeEach(async () => {
            const fullPath = path.resolve(__dirname, "fakePackageCompatible");
            result = await arePackagesInstalled(fullPath, 2, false);
        });

        test("should display proper messages", () => {
            const yellowChalkCode = getChalkYellowCode();
            expect(console.log.mock.calls.length).toBe(2);
            expect(console.log.mock.calls[0][0]).toContain(
                `${yellowChalkCode}Please remember to install/update packages, otherwise the results may be incorrect.`
            );
            expect(console.log.mock.calls[1][0]).toEqual(
                "Number of packages found by Solicitorio: 2 (expected: 2+)."
            );
        });

        test("should return true", () => {
            expect(result).toBe(true);
        });
    });

    describe("when number of found packages is less than expected", () => {
        describe("when prompt was skipped", () => {
            describe("when node_modules folder was not found", () => {
                const fullPath = path.resolve(__dirname, "fakePackageMissingNodeModules");
                let result;

                beforeEach(async () => {
                    result = await arePackagesInstalled(fullPath, 1, true);
                });

                test("should display correct number of messages", () => {
                    expect(console.log.mock.calls.length).toBe(4);
                });

                test("should display warning message", () => {
                    expect(console.log.mock.calls[0][0]).toContain(
                        "Please remember to install/update packages, otherwise the results may be incorrect."
                    );
                });

                test("should display message about number of found packages", () => {
                    expect(console.log.mock.calls[1][0]).toContain(
                        "Number of packages found by Solicitorio: 1 (expected: 2+)."
                    );
                });

                test("should display warning about missing node_modules", () => {
                    expect(console.log.mock.calls[2][0]).toContain(
                        "node_modules directory is missing."
                    );
                });

                test("should display answered prompt", () => {
                    expect(console.log.mock.calls[3][0]).toContain(
                        "Are you sure, that all packages are installed? (y/n): yes"
                    );
                });

                test("should return true", () => {
                    expect(result).toBe(true);
                });
            });

            describe("when node_modules folder was found", () => {
                const fullPath = path.resolve(__dirname, "fakePackageExistingNodeModules");
                const nodeModulesPath = path.join(fullPath, "node_modules");

                beforeAll(async () => {
                    mkdirSync(nodeModulesPath);
                });

                afterAll(() => {
                    rmdirSync(nodeModulesPath);
                });

                beforeEach(async () => {
                    result = await arePackagesInstalled(fullPath, 1, true);
                });

                test("should display correct number of messages", () => {
                    expect(console.log.mock.calls.length).toBe(3);
                });

                test("should display warning message", () => {
                    expect(console.log.mock.calls[0][0]).toContain(
                        "Please remember to install/update packages, otherwise the results may be incorrect."
                    );
                });

                test("should display message about number of found packages", () => {
                    expect(console.log.mock.calls[1][0]).toContain(
                        "Number of packages found by Solicitorio: 1 (expected: 2+)."
                    );
                });

                test("should display answered prompt", () => {
                    expect(console.log.mock.calls[2][0]).toContain(
                        "Are you sure, that all packages are installed? (y/n): yes"
                    );
                });

                test("should return true", () => {
                    expect(result).toBe(true);
                });
            });
        });

        describe("when prompt was not skipped", () => {
            let promptText;
            let promptAnswer;

            beforeEach(() => {
                jest.spyOn(readline, 'createInterface').mockImplementation(() => {
                    return {
                        question: (text, callback) => {
                            promptText = text;
                            callback(promptAnswer);
                        },
                        close: () => {}
                    }
                })
            });

            describe("when node_modules folder was not found", () => {
                const fullPath = path.resolve(__dirname, "fakePackageMissingNodeModules");

                describe("when prompt was confirmed", () => {
                    let result;

                    beforeEach(async () => {
                        promptAnswer = "yes";
                        result = await arePackagesInstalled(fullPath, 1, false);
                    });

                    test("should display correct number of messages", () => {
                        expect(console.log.mock.calls.length).toBe(3);
                    });

                    test("should display warning message", () => {
                        expect(console.log.mock.calls[0][0]).toContain(
                            "Please remember to install/update packages, otherwise the results may be incorrect."
                        );
                    });

                    test("should display message about number of found packages", () => {
                        expect(console.log.mock.calls[1][0]).toContain(
                            "Number of packages found by Solicitorio: 1 (expected: 2+)."
                        );
                    });

                    test("should display warning about missing node_modules", () => {
                        expect(console.log.mock.calls[2][0]).toContain(
                            "node_modules directory is missing."
                        );
                    });

                    test("should display correct prompt", () => {
                        expect(promptText).toBe("Are you sure, that all packages are installed? (y/n): ");
                    });

                    test("should return true", () => {
                        expect(result).toBe(true);
                    });
                });

                describe("when prompt was not confirmed", () => {
                    let result;

                    beforeEach(async () => {
                        promptAnswer = "no";
                        result = await arePackagesInstalled(fullPath, 1, false);
                    });

                    test("should display correct number of messages", () => {
                        expect(console.log.mock.calls.length).toBe(3);
                    });

                    test("should display warning message", () => {
                        expect(console.log.mock.calls[0][0]).toContain(
                            "Please remember to install/update packages, otherwise the results may be incorrect."
                        );
                    });

                    test("should display message about number of found packages", () => {
                        expect(console.log.mock.calls[1][0]).toContain(
                            "Number of packages found by Solicitorio: 1 (expected: 2+)."
                        );
                    });

                    test("should display warning about missing node_modules", () => {
                        expect(console.log.mock.calls[2][0]).toContain(
                            "node_modules directory is missing."
                        );
                    });

                    test("should display correct prompt", () => {
                        expect(promptText).toBe("Are you sure, that all packages are installed? (y/n): ");
                    });

                    test("should return false", () => {
                        expect(result).toBe(false);
                    });
                });
            });

            describe("when node_modules folder was found", () => {
                const fullPath = path.resolve(__dirname, "fakePackageExistingNodeModules");
                const nodeModulesPath = path.join(fullPath, "node_modules");

                beforeAll(() => {
                    mkdirSync(nodeModulesPath);
                });

                afterAll(() => {
                    rmdirSync(nodeModulesPath);
                });

                describe("when prompt was confirmed", () => {
                    let result;

                    beforeEach(async () => {
                        promptAnswer = "yes";
                        result = await arePackagesInstalled(fullPath, 1, false);
                    });

                    test("should display correct number of messages", () => {
                        expect(console.log.mock.calls.length).toBe(2);
                    });

                    test("should display warning message", () => {
                        expect(console.log.mock.calls[0][0]).toContain(
                            "Please remember to install/update packages, otherwise the results may be incorrect."
                        );
                    });

                    test("should display message about number of found packages", () => {
                        expect(console.log.mock.calls[1][0]).toContain(
                            "Number of packages found by Solicitorio: 1 (expected: 2+)."
                        );
                    });

                    test("should display correct prompt", () => {
                        expect(promptText).toBe("Are you sure, that all packages are installed? (y/n): ");
                    });

                    test("should return true", () => {
                        expect(result).toBe(true);
                    });
                });

                describe("when prompt was not confirmed", () => {
                    let result;

                    beforeEach(async () => {
                        promptAnswer = "no";
                        result = await arePackagesInstalled(fullPath, 1, false);
                    });

                    test("should display correct number of messages", () => {
                        expect(console.log.mock.calls.length).toBe(2);
                    });

                    test("should display warning message", () => {
                        expect(console.log.mock.calls[0][0]).toContain(
                            "Please remember to install/update packages, otherwise the results may be incorrect."
                        );
                    });

                    test("should display message about number of found packages", () => {
                        expect(console.log.mock.calls[1][0]).toContain(
                            "Number of packages found by Solicitorio: 1 (expected: 2+)."
                        );
                    });

                    test("should display correct prompt", () => {
                        expect(promptText).toBe("Are you sure, that all packages are installed? (y/n): ");
                    });

                    test("should return false", () => {
                        expect(result).toBe(false);
                    });
                });
            });
        })
    });

    describe("when working with a workspace with no direct dependencies", () => {
        const fullPath = path.resolve(__dirname, "fakeWorkspace");
        beforeEach(async () => {
            result = await arePackagesInstalled(fullPath, 1, true);
        });

        test("should increase the expected minimum count by 1", () => {
            expect(console.log.mock.calls[1][0]).toContain(
                "Number of packages found by Solicitorio: 1 (expected: 2+)."
            );
        });
    });

    describe("when working with a package without a version", () => {
        const fullPath = path.resolve(__dirname, "fakePackageNoVersion");
        beforeEach(async () => {
            result = await arePackagesInstalled(fullPath, 1, true);
        });

        test("should issue a warning about the missing `version` field", () => {
            expect(console.warn.mock.calls[0][0]).toMatch (
                /Warning: .*\/fakePackageNoVersion\/package.json does not specify a 'version' which will result in a decreased package count/
            );
        });
    });
});
