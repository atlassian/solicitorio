This document explains the overrides applied to the dependencies of our project due to identified vulnerabilities. 
It provides clarity on why these overrides were necessary and under what conditions they can be removed.

### **/read-package-json/glob
https://asecurityteam.atlassian.net/browse/VULN-1432915
@atlassian/solicitorio@3.0.3 > license-checker@25.0.1 > read-installed@4.0.3 > read-package-json@2.1.2 > glob@7.2.3 > inflight@1.0.6

The read-installed and read-package-json are officially deprecated on npmjs.

We addressed the VULN in two steps:
1. Override `glob` for `read-package-json`
2. Patch read-package-json to adjust to the breaking changes between `glob` 9 and 8

